#!/usr/bin/env python

from sklearn.cross_validation import KFold
from sklearn.linear_model import LinearRegression as LR
from sklearn.metrics import mean_absolute_error as MAE
from sklearn.preprocessing import StandardScaler

import argparse
import logging
import numpy as np
import os
import time

from kaggler.data_io import load_data
from const import N_FOLD, SEED


def train_predict_lr_forward(train_file, test_file, predict_valid_file,
                             predict_test_file, retrain=False):

    model_name = os.path.splitext(os.path.splitext(os.path.basename(predict_test_file))[0])[0]
    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG,
                        filename='{}.log'.format(model_name))

    logging.info("Loading training and test data...")
    X_trn, y = load_data(train_file, dense=True)
    X_tst, _ = load_data(test_file, dense=True)

    logging.info('Normalizing data')
    X_trn[X_trn < 0.] = 0.
    X_tst[X_tst < 0.] = 0.

    cv = KFold(len(y), n_folds=N_FOLD, shuffle=True, random_state=SEED)
    clf = LR()

    selected_features = []
    features_to_test = [x for x in range(X_trn.shape[1])
                        if x not in selected_features]

    MAE_cv_old = np.inf
    is_improving = True
    while is_improving and features_to_test:
        MAE_cvs = []
        for feature in features_to_test:
            logging.info('{}'.format(selected_features + [feature]))
            X = X_trn[:, selected_features + [feature]]

            p_val = np.zeros((X_trn.shape[0], ))
            for i, (i_trn, i_val) in enumerate(cv, start=1):
                clf.fit(X[i_trn], y[i_trn])
                p_val[i_val] = clf.predict(X[i_val])

            MAE_cv = MAE(y, p_val)
            logging.info('MAE CV: {:.6f}'.format(MAE_cv))
            MAE_cvs.append(MAE_cv)

        MAE_cv_new = min(MAE_cvs)
        if MAE_cv_new < MAE_cv_old:
            MAE_cv_old = MAE_cv_new
            feature = features_to_test.pop(MAE_cvs.index(MAE_cv_new))
            selected_features.append(feature)
            logging.info('selected features: {}'.format(selected_features))
        else:
            is_improving = False
            logging.info('final selected features: {}'.format(selected_features))

    logging.info('saving selected features as a file')
    with open('{}_selected.txt'.format(model_name), 'w') as f:
        f.write('{}\n'.format(selected_features))
        f.write('{}\n'.format(clf.coef_))

    X = X_trn[:, selected_features]
    logging.debug('feature matrix: {}x{}'.format(X.shape[0], X.shape[1]))

    p_val = np.zeros((X_trn.shape[0], ))
    p_tst = np.zeros((X_tst.shape[0], ))
    for i, (i_trn, i_val) in enumerate(cv, start=1):
        logging.info('Training CV #{}'.format(i))
        clf.fit(X[i_trn], y[i_trn])
        p_val[i_val] = clf.predict(X[i_val])
        p_tst += clf.predict(X_tst[:, selected_features]) / N_FOLD

    MAE_cv = MAE(y, p_val)
    logging.info('MAE CV: {:.6f}'.format(MAE_cv))
    logging.info("Writing test predictions to file")
    np.savetxt(predict_valid_file, p_val, fmt='%.6f')

    if retrain:
        logging.info('Retraining with 100% data...')
        clf.fit(X, y)
        p_tst = clf.predict(X_tst[:, selected_features])

    np.savetxt(predict_test_file, p_tst, fmt='%.6f')

    
if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file', '-t', required=True, dest='train')
    parser.add_argument('--test-file', '-v', required=True, dest='test')
    parser.add_argument('--predict-valid-file', '-p', required=True,
                        dest='predict_train')
    parser.add_argument('--predict-test-file', '-q', required=True,
                        dest='predict_test')

    args = parser.parse_args()

    start = time.time()
    train_predict_lr_forward(train_file=args.train,
                             test_file=args.test,
                             predict_valid_file=args.predict_train,
                             predict_test_file=args.predict_test)

    logging.info('Finished ({:.2f} sec elasped).'.format(time.time() - start))
