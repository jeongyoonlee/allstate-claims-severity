#!/usr/bin/env python

from __future__ import division
from sklearn.cross_validation import KFold
from sklearn.metrics import mean_absolute_error as MAE
from sklearn.ensemble import ExtraTreesRegressor as ET

import argparse
import logging
import numpy as np
import os
import time

from const import N_FOLD, SEED
from kaggler.data_io import load_data


offset = 200


def train_predict(train_file, test_file, predict_valid_file, predict_test_file,
                  n_est, depth):

    model_name = os.path.splitext(os.path.splitext(os.path.basename(predict_test_file))[0])[0]
    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG,
                        filename='{}.log'.format(model_name))


    logging.info('Loading training and test data...')
    X, y = load_data(train_file, dense=True)
    y = np.log(y + offset)
    X_tst, _ = load_data(test_file, dense=True)

    clf = ET(n_estimators=n_est, max_depth=depth, random_state=SEED)

    logging.info('Loading CV Ids')
    cv = KFold(len(y), n_folds=N_FOLD, shuffle=True, random_state=SEED)

    logging.info('Cross validation...')
    p_val = np.zeros_like(y)
    p_tst = np.zeros((X_tst.shape[0],))
    for i, (i_trn, i_val) in enumerate(cv, 1):
        clf.fit(X[i_trn], y[i_trn])
        p_val[i_val] = clf.predict(X[i_val])
        p_tst += clf.predict(X_tst) / N_FOLD
        logging.info('CV MAE #{}: {:.6f}'.format(i, MAE(np.exp(p_val[i_trn]),
                                                        np.exp(y[i_trn]))))

    logging.info('CV MAE: {:.6f}'.format(MAE(np.exp(p_val), np.exp(y))))

    logging.info('Saving predictions...')
    np.savetxt(predict_valid_file, np.exp(p_val) - offset, fmt='%.6f', delimiter=',')
    np.savetxt(predict_test_file, np.exp(p_tst) - offset, fmt='%.6f', delimiter=',')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file', required=True, dest='train_file')
    parser.add_argument('--test-file', required=True, dest='test_file')
    parser.add_argument('--predict-valid-file', required=True,
                        dest='predict_valid_file')
    parser.add_argument('--predict-test-file', required=True,
                        dest='predict_test_file')
    parser.add_argument('--n-est', default=100, type=int, dest='n_est')
    parser.add_argument('--depth', default=None, type=int, dest='depth')

    args = parser.parse_args()

    start = time.time()
    train_predict(train_file=args.train_file,
                  test_file=args.test_file,
                  predict_valid_file=args.predict_valid_file,
                  predict_test_file=args.predict_test_file,
                  n_est=args.n_est,
                  depth=args.depth)
    logging.info('finished ({:.2f} min elasped)'.format((time.time() - start) /
                                                        60))
