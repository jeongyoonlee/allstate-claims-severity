#!/usr/bin/env python

from __future__ import division
from scipy import sparse
from sklearn.cross_validation import KFold
from sklearn.datasets import dump_svmlight_file
from sklearn.metrics import mean_absolute_error as MAE

import argparse
import logging
import numpy as np
import os
import subprocess
import time

from const import N_FOLD, SEED
from kaggler.data_io import load_data, save_data


offset = 200


def train_predict(train_file, test_file, predict_valid_file, predict_test_file,
                  n_iter=100, dim=4, lrate=.1):

    dir_feature = os.path.dirname(train_file)
    dir_val = os.path.dirname(predict_valid_file)
    dir_tst = os.path.dirname(predict_test_file)

    feature_name = os.path.splitext(os.path.splitext(os.path.basename(train_file))[0])[0]
    model_name = os.path.splitext(os.path.splitext(os.path.basename(predict_test_file))[0])[0]

    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG, filename='{}.log'.format(model_name))

    logging.info('Loading training data')
    X, y = load_data(train_file)
    y = np.log(y + offset)
    X_tst, _ = load_data(test_file)

    cv = KFold(len(y), n_folds=N_FOLD, shuffle=True, random_state=SEED)

    p_val = np.zeros_like(y)
    p_tst = np.zeros((X_tst.shape[0],))
    for i, (i_trn, i_val) in enumerate(cv, 1):
        logging.info('Training model #{}...'.format(i))
        val_trn_file = os.path.join(dir_feature, '{}.trn{}.sps'.format(feature_name, i))
        val_tst_file = os.path.join(dir_feature, '{}.val{}+tst.sps'.format(feature_name, i))
        predict_file = os.path.join(dir_val, '{}.val+tst{}.yht'.format(model_name, i))

        # if there is no CV training or validation file, then generate them
        # first.
        if not os.path.isfile(val_trn_file):
            save_data(X[i_trn], y[i_trn], val_trn_file)

        if not os.path.isfile(val_tst_file):
            if sparse.issparse(X):
                X_valtst = sparse.vstack((X[i_val], X_tst))
            else:
                X_valtst = np.vstack((X[i_val], X_tst))
            save_data(X_valtst, None, val_tst_file)

        subprocess.call(["libFM",
                         "-task", "r",
                         '-dim', '1,1,{}'.format(dim),
                         '-init_stdev', str(lrate),
                         '-iter', str(n_iter),
                         '-train', val_trn_file,
                         '-test', val_tst_file,
                         '-out', predict_file])

        p = np.loadtxt(predict_file)

        n_val = len(i_val)
        p_val[i_val] = p[:n_val]
        p_tst += p[n_val:] / N_FOLD

        os.remove(predict_file)

    logging.info('MAE = {:.6f}'.format(MAE(np.exp(y), np.exp(p_val))))
    np.savetxt(predict_valid_file, np.exp(p_val) - offset, fmt='%.6f')
    np.savetxt(predict_test_file, np.exp(p_tst) - offset, fmt='%.6f')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file', required=True, dest='train_file')
    parser.add_argument('--test-file', required=True, dest='test_file')
    parser.add_argument('--predict-valid-file', required=True,
                        dest='predict_valid_file')
    parser.add_argument('--predict-test-file', required=True,
                        dest='predict_test_file')
    parser.add_argument('--n-iter', type=int, dest='n_iter')
    parser.add_argument('--dim', type=int, dest='dim')
    parser.add_argument('--lrate', type=float, dest='lrate')

    args = parser.parse_args()

    start = time.time()
    train_predict(train_file=args.train_file,
                  test_file=args.test_file,
                  predict_valid_file=args.predict_valid_file,
                  predict_test_file=args.predict_test_file,
                  n_iter=args.n_iter,
                  dim=args.dim,
                  lrate=args.lrate)
    logging.info('finished ({:.2f} min elasped)'.format((time.time() - start) /
                                                        60))
