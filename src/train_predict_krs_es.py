#!/usr/bin/env python
from __future__ import absolute_import, division, print_function
from keras.callbacks import EarlyStopping
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.layers.normalization import BatchNormalization
from keras.layers.advanced_activations import PReLU
from keras.utils import np_utils
from scipy import sparse
from sklearn.preprocessing import LabelEncoder, OneHotEncoder, StandardScaler
from sklearn.cross_validation import KFold
from sklearn.metrics import mean_absolute_error as MAE

import argparse
import logging
import numpy as np
import os
import pandas as pd
import time
import keras.backend as K

from kaggler.data_io import load_data
from const import N_CLASS, N_FOLD, SEED


np.random.seed(SEED)
offset = 200


def mae(y, p):
    return K.mean(K.abs(K.exp(y) - K.exp(p)))


def get_model(nb_classes, dims, hiddens=2, neurons=512, dropout=0.5):
    model = Sequential()
    model.add(Dense(neurons, input_dim=dims, init='he_normal'))
    model.add(Activation('relu'))
    model.add(BatchNormalization())
    model.add(Dropout(dropout))

    for i in range(hiddens):
        model.add(Dense(neurons // (2 ** i), init='he_normal'))
        model.add(Activation('relu'))
        model.add(BatchNormalization())
        model.add(Dropout(dropout))

    model.add(Dense(nb_classes, init='he_normal'))
    model.compile(loss='mae', optimizer="adadelta", metrics=[mae])

    return model


def train_predict(train_file, test_file, predict_valid_file, predict_test_file,
                  n_est=100, hiddens=2, neurons=512, dropout=0.5, batch=16,
                  n_stop=2, retrain=False):

    model_name = os.path.splitext(os.path.splitext(os.path.basename(predict_test_file))[0])[0]

    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG,
                        filename='{}.log'.format(model_name))

    logging.info('Loading training and test data...')
    X, y = load_data(train_file, dense=True)
    y = np.log(y + offset)

    X_tst, _ = load_data(test_file, dense=True)

    if sparse.issparse(X):
        X = X.todense()
        X_tst = X_tst.todense()

    scaler = StandardScaler()
    X = scaler.fit_transform(X)
    X_tst = scaler.transform(X_tst)

    dims = X.shape[1]
    logging.info('{} classes, {} dims'.format(N_CLASS, dims))

    logging.info('Loading CV Ids')
    cv = KFold(len(y), n_folds=N_FOLD, shuffle=True, random_state=SEED)

    p_val = np.zeros_like(y)
    p_tst = np.zeros((X_tst.shape[0], ))
    for i, (i_trn, i_val) in enumerate(cv, 1):
        logging.info('Training model #{}'.format(i))
        clf = get_model(N_CLASS, dims, hiddens, neurons, dropout)
        if i == 1:
            early_stopping = EarlyStopping(monitor='val_mae', patience=n_stop)
            h = clf.fit(X[i_trn],
                        y[i_trn],
                        validation_data=(X[i_val], y[i_val]),
                        nb_epoch=n_est,
                        batch_size=batch,
                        callbacks=[early_stopping])

            val_maes = h.history['val_mae']
            n_best = val_maes.index(min(val_maes)) + 1
            logging.info('best epoch={}'.format(n_best))
        else:
            clf.fit(X[i_trn],
                    y[i_trn],
                    validation_data=(X[i_val], y[i_val]),
                    nb_epoch=n_best,
                    batch_size=batch)

        p_val[i_val] = clf.predict(X[i_val])
        logging.info('CV #{} MAE: {:.6f}'.format(i, MAE(np.exp(y[i_val]),
                                                        np.exp(p_val[i_val]))))

        if not retrain:
            p_tst += clf.predict(X_tst).reshape(-1) / N_FOLD

    logging.info('Saving normalized validation predictions...')
    logging.info('CV MAE: {:.6f}'.format(MAE(y, p_val)))
    np.savetxt(predict_valid_file, np.exp(p_val) - offset, fmt='%.6f', delimiter=',')

    if retrain:
        logging.info('Retraining with 100% training data')
        clf = get_model(N_CLASS, dims, hiddens, neurons, dropout)
        clf.fit(X, y, nb_epoch=n_best, batch_size=batch)
        p_tst = clf.predict(X_tst).reshape(-1)

    logging.info('Saving normalized test predictions...')
    np.savetxt(predict_test_file, np.exp(p_tst) - offset, fmt='%.6f', delimiter=',')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file', required=True, dest='train_file')
    parser.add_argument('--test-file', required=True, dest='test_file')
    parser.add_argument('--predict-valid-file', required=True,
                        dest='predict_valid_file')
    parser.add_argument('--predict-test-file', required=True,
                        dest='predict_test_file')
    parser.add_argument('--n-est', default=10, type=int, dest='n_est')
    parser.add_argument('--batch-size', default=64, type=int,
                        dest='batch_size')
    parser.add_argument('--hiddens', default=2, type=int)
    parser.add_argument('--neurons', default=512, type=int)
    parser.add_argument('--dropout', default=0.5, type=float)
    parser.add_argument('--early-stopping', default=2, type=int, dest='n_stop')
    parser.add_argument('--retrain', default=False, action='store_true')

    args = parser.parse_args()

    start = time.time()
    train_predict(train_file=args.train_file,
                  test_file=args.test_file,
                  predict_valid_file=args.predict_valid_file,
                  predict_test_file=args.predict_test_file,
                  n_est=args.n_est,
                  neurons=args.neurons,
                  dropout=args.dropout,
                  batch=args.batch_size,
                  hiddens=args.hiddens,
                  n_stop=args.n_stop,
                  retrain=args.retrain)
    logging.info('finished ({:.2f} min elasped)'.format((time.time() - start) /
                                                        60))

