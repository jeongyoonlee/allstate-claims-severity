#!/usr/bin/env python
from __future__ import division
from pymc3 import Model, glm, NUTS, sample
from sklearn.cross_validation import KFold
from sklearn.metrics import mean_absolute_error as MAE
from sklearn.preprocessing import StandardScaler

import argparse
import logging
import numpy as np
import os
import time

from kaggler.data_io import load_data
from const import SEED


offset = 200


def train_predict(train_file, test_file, predict_valid_file, predict_test_file, retrain=False):

    model_name = os.path.splitext(os.path.splitext(os.path.basename(predict_test_file))[0])[0]
    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG,
                        filename='{}.log'.format(model_name))

    logging.info('Loading training and test data...')
    X, y = load_data(train_file)
    y = np.log(y + offset)

    X_tst, _ = load_data(test_file)

    logging.info('Normalizing data')
    X = np.log(X + offset)
    X_tst = np.log(X_tst + offset)

    data = dict(y=y)
    for col in range(X.shape[1]):
        data['X{}'.format(col)] = X[:, col]

    logging.info('Training model...')
    with Model() as model:
        glm.glm('y ~ {}'.format('+'.join(['X{}'.format(col) for col in range(X.shape[1])])), data)
        step = NUTS()
        trace = sample(500, step, progressbar=False)

        intercept = np.median(trace.Intercept)
        coefs = np.array([np.median(eval('trace.X{}'.format(col))) for col in range(X.shape[1])])

    logging.info('coefficients: {}'.format(coefs))
    np.savetxt('{}.txt'.format(model_name), coefs, fmt='%.6f', delimiter=',')
    p_val = (X * coefs).sum(axis=1) + intercept
    p_tst = (X_tst * coefs).sum(axis=1) + intercept

    logging.info('MAE = {:.6f}'.format(MAE(np.exp(y), np.exp(p_val))))
    np.savetxt(predict_valid_file, np.exp(p_val) - offset, fmt='%.6f')

    logging.info('Saving predictions...')
    np.savetxt(predict_test_file, np.exp(p_tst) - offset, fmt='%.6f')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file', required=True, dest='train_file')
    parser.add_argument('--test-file', required=True, dest='test_file')
    parser.add_argument('--predict-valid-file', required=True,
                        dest='predict_valid_file')
    parser.add_argument('--predict-test-file', required=True,
                        dest='predict_test_file')

    args = parser.parse_args()

    start = time.time()
    train_predict(train_file=args.train_file,
                  test_file=args.test_file,
                  predict_valid_file=args.predict_valid_file,
                  predict_test_file=args.predict_test_file)
    logging.info('finished ({:.2f} min elasped)'.format((time.time() - start) /
                                                        60))
