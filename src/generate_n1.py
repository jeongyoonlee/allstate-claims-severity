#!/usr/bin/env python
from __future__ import division
from itertools import combinations
from scipy import sparse
from scipy.stats import skew, boxcox
from sklearn.preprocessing import minmax_scale, StandardScaler
import argparse
import logging
import numpy as np
import os
import pandas as pd
import time

from kaggler.data_io import load_data, save_data
from kaggler.preprocessing import OneHotEncoder, Normalizer


def generate_feature(train_file, test_file, train_feature_file,
                     test_feature_file, feature_map_file):
    logging.info('loading raw data')
    trn = pd.read_csv(train_file, index_col='id')
    tst = pd.read_csv(test_file, index_col='id')

    y = trn.loss.values
    n_trn = trn.shape[0]

    trn.drop('loss', axis=1, inplace=True)

    cat_cols = [x for x in trn.columns if trn[x].dtype == np.object]
    num_cols = [x for x in trn.columns if trn[x].dtype != np.object]

    logging.info('categorical: {}, numerical: {}'.format(len(cat_cols),
                                                         len(num_cols)))

    df = pd.concat([trn, tst], axis=0)

    # mungeskewed()
    for col in num_cols:
        if skew(df[col].dropna()) > .25:
            df.ix[:, col], _ = boxcox(df[col] + 1)

    for column in cat_cols:
        if trn[column].nunique() != tst[column].nunique():
            set_train = set(trn[column].unique())
            set_test = set(tst[column].unique())
            remove_train = set_train - set_test
            remove_test = set_test - set_train

            remove = remove_train.union(remove_test)

            def filter_cat(x):
                if x in remove:
                    return np.nan
                return x

            df[column] = df[column].apply(lambda x: filter_cat(x))

    df["cont1"] = np.sqrt(minmax_scale(df["cont1"]))
    df["cont4"] = np.sqrt(minmax_scale(df["cont4"]))
    df["cont5"] = np.sqrt(minmax_scale(df["cont5"]))
    df["cont8"] = np.sqrt(minmax_scale(df["cont8"]))
    df["cont10"] = np.sqrt(minmax_scale(df["cont10"]))
    df["cont11"] = np.sqrt(minmax_scale(df["cont11"]))
    df["cont12"] = np.sqrt(minmax_scale(df["cont12"]))

    df["cont6"] = np.log(minmax_scale(df["cont6"]) + .1)
    df["cont7"] = np.log(minmax_scale(df["cont7"]) + .1)
    df["cont9"] = np.log(minmax_scale(df["cont9"]) + .1)
    df["cont13"] = np.log(minmax_scale(df["cont13"]) + .1)
    df["cont14"] = (np.maximum(df["cont14"] - .18, 0) / .67) ** 0.25

    logging.info('label encoding categorical variables')
    ohe = OneHotEncoder(min_obs=10)
    X_ohe = ohe.fit_transform(df[cat_cols].values)
    ohe_cols = ['ohe{}'.format(i) for i in range(X_ohe.shape[1])]

    logging.info('Normalizing numeric features')
    ss = StandardScaler()
    df[num_cols] = ss.fit_transform(df[num_cols].values)

    X = sparse.hstack((df[num_cols].values, X_ohe), format='csr')

    with open(feature_map_file, 'w') as f:
        for i, col in enumerate(num_cols + ohe_cols):
            f.write('{}\t{}\tq\n'.format(i, col))

    logging.info('saving features')
    save_data(X[:n_trn,], y, train_feature_file)
    save_data(X[n_trn:,], None, test_feature_file)


if __name__ == '__main__':

    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file', required=True, dest='train_file')
    parser.add_argument('--test-file', required=True, dest='test_file')
    parser.add_argument('--train-feature-file', required=True, dest='train_feature_file')
    parser.add_argument('--test-feature-file', required=True, dest='test_feature_file')
    parser.add_argument('--feature-map-file', required=True, dest='feature_map_file')

    args = parser.parse_args()

    start = time.time()
    generate_feature(args.train_file,
                     args.test_file,
                     args.train_feature_file,
                     args.test_feature_file,
                     args.feature_map_file)
    logging.info('finished ({:.2f} sec elasped)'.format(time.time() - start))

