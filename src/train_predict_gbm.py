#!/usr/bin/env python

from __future__ import division
from sklearn.cross_validation import KFold
from sklearn.metrics import mean_absolute_error as MAE
from sklearn.ensemble import GradientBoostingRegressor as GBM

import argparse
import logging
import numpy as np
import os
import time

from const import N_FOLD, SEED
from kaggler.data_io import load_data


def train_predict(train_file, test_file, predict_valid_file, predict_test_file,
                  n_est, lrate, subrow, depth):

    model_name = os.path.splitext(os.path.splitext(os.path.basename(predict_test_file))[0])[0]
    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG,
                        filename='{}.log'.format(model_name))


    logging.info('Loading training and test data...')
    X, y = load_data(train_file, dense=True)
    X_tst, _ = load_data(test_file, dense=True)

    clf = GBM(loss='lad', learning_rate=lrate, n_estimators=n_est,
              subsample=subrow, criterion='mae', max_depth=depth,
              random_state=SEED)

    logging.info('Loading CV Ids')
    cv = KFold(len(y), n_folds=N_FOLD, shuffle=True, random_state=SEED)

    logging.info('Cross validation...')
    p_val = np.zeros_like(y)
    p_tst = np.zeros((X_tst.shape[0],))
    for i, (i_trn, i_val) in enumerate(cv, 1):
        clf.fit(X[i_trn], y[i_trn])
        p_val[i_val] = clf.predict(X[i_val])
        p_tst += clf.predict(X_tst) / N_FOLD
        logging.info('CV MAE #{}: {:.6f}'.format(i, MAE(p_val[i_trn], y[i_trn])))

    logging.info('CV MAE: {:.6f}'.format(MAE(p_val, y)))

    logging.info('Saving predictions...')
    np.savetxt(predict_valid_file, p_val, fmt='%.6f', delimiter=',')
    np.savetxt(predict_test_file, p_tst, fmt='%.6f', delimiter=',')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file', required=True, dest='train_file')
    parser.add_argument('--test-file', required=True, dest='test_file')
    parser.add_argument('--predict-valid-file', required=True,
                        dest='predict_valid_file')
    parser.add_argument('--predict-test-file', required=True,
                        dest='predict_test_file')
    parser.add_argument('--n-est', default=100, type=int, dest='n_est')
    parser.add_argument('--depth', default=None, type=int)
    parser.add_argument('--lrate', default=.01, type=float)
    parser.add_argument('--subrow', default=.5, type=float)

    args = parser.parse_args()

    start = time.time()
    train_predict(train_file=args.train_file,
                  test_file=args.test_file,
                  predict_valid_file=args.predict_valid_file,
                  predict_test_file=args.predict_test_file,
                  n_est=args.n_est,
                  lrate=args.lrate,
                  subrow=args.subrow,
                  depth=args.depth)
    logging.info('finished ({:.2f} min elasped)'.format((time.time() - start) /
                                                        60))
