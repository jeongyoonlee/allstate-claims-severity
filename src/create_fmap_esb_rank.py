import argparse


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--base-feature-map-file', required=True,
                        dest='base_feature_map_file')
    parser.add_argument('--feature-map-file', required=True,
                        dest='feature_map_file')

    args = parser.parse_args()

    with open(args.feature_map_file, 'w') as fout, \
         open(args.base_feature_map_file) as fin:
        for i, row in enumerate(fin):
            fout.write(row)

        n = i + 1
        for i in range(n):
            fout.write('{}\t{}\tq\n'.format(n + i, i))

