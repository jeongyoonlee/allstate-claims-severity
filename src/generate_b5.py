#!/usr/bin/env python
from __future__ import division
from itertools import combinations
from scipy.stats import skew, boxcox
import argparse
import logging
import numpy as np
import os
import pandas as pd
import time

from kaggler.data_io import load_data, save_data
from kaggler.preprocessing import LabelEncoder, Normalizer


imp_cat_cols = ('cat100,cat112,cat116,cat113,cat110,cat101,cat114,cat115,'
                'cat91,cat83,cat107,cat105,cat84,cat93,'
                'cat80,cat87,cat57,cat12,cat79,cat10,cat7,cat89,cat2,cat72,'
                'cat81,cat11,cat1,cat13,cat9,cat3,cat16,cat90,cat23,cat36,'
                'cat73,cat103,cat40,cat28,cat111,cat6,cat76,cat50,cat5,'
                'cat4,cat14,cat38,cat24,cat82,cat25').split(',')

with open('build/feature/b5') as f:
    selected_cols = f.read().split('\n')


def encode(charcode):
    r = 0
    ln = len(charcode)
    for i in range(ln):
        r += (ord(charcode[i])-ord('A')+1)*26**(ln-i-1)
    return r


def generate_feature(train_file, test_file, train_feature_file,
                     test_feature_file, feature_map_file):
    logging.info('loading raw data')
    trn = pd.read_csv(train_file, index_col='id')
    tst = pd.read_csv(test_file, index_col='id')

    y = trn.loss.values
    n_trn = trn.shape[0]

    trn.drop('loss', axis=1, inplace=True)

    cat_cols = [x for x in trn.columns if trn[x].dtype == np.object]
    num_cols = [x for x in trn.columns if trn[x].dtype != np.object]

    logging.info('categorical: {}, numerical: {}'.format(len(cat_cols),
                                                         len(num_cols)))

    df = pd.concat([trn, tst], axis=0)

    for i, (col1, col2) in enumerate(combinations(imp_cat_cols, 2), 1):
        col = '{}_{}'.format(col1, col2)
        if col in selected_cols:
            df.ix[:, col] = df[col1] + df[col2]
            cat_cols.append(col)
     
            if (i % 10) == 0:
                logging.info('{} interactions processed'.format(i))

    logging.info('{} interactions processed'.format(i))
    logging.info('2nd order interaction categorical: {}'.format(len(cat_cols)))

    logging.info('label encoding categorical variables')
    for col in cat_cols:
        df[col] = df[col].apply(encode)

    logging.info('adding 2nd order interactions for numeric features')
    for col1, col2 in combinations(num_cols, 2):
        col = '{}+{}'.format(col1, col2)
        if col in selected_cols:
            df.ix[:, col] = df[col1] + df[col2]
            num_cols.append(col)

        col = '{}-{}'.format(col1, col2)
        if col in selected_cols:
            df.ix[:, col] = df[col1] - df[col2]
            num_cols.append(col)

        col = '{}*{}'.format(col1, col2)
        if col in selected_cols:
            df.ix[:, col] = df[col1] * df[col2]
            num_cols.append(col)

        col = '{}/{}'.format(col1, col2)
        if col in selected_cols:
            df.ix[:, col] = df[col1] / (df[col2] + 1e-5)
            num_cols.append(col)

    logging.info('2nd order interaction categorical: {}'.format(len(num_cols)))

    logging.info('normalizing numeric features')
    nm = Normalizer()
    df.ix[:, num_cols] = nm.fit_transform(df[num_cols].values)

    with open(feature_map_file, 'w') as f:
        for i, col in enumerate(df.columns):
            f.write('{}\t{}\tq\n'.format(i, col))

    logging.info('saving features')
    save_data(df.values[:n_trn,], y, train_feature_file)
    save_data(df.values[n_trn:,], None, test_feature_file)


if __name__ == '__main__':

    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file', required=True, dest='train_file')
    parser.add_argument('--test-file', required=True, dest='test_file')
    parser.add_argument('--train-feature-file', required=True, dest='train_feature_file')
    parser.add_argument('--test-feature-file', required=True, dest='test_feature_file')
    parser.add_argument('--feature-map-file', required=True, dest='feature_map_file')

    args = parser.parse_args()

    start = time.time()
    generate_feature(args.train_file,
                     args.test_file,
                     args.train_feature_file,
                     args.test_feature_file,
                     args.feature_map_file)
    logging.info('finished ({:.2f} sec elasped)'.format(time.time() - start))

