#!/usr/bin/env python
from __future__ import division
from itertools import combinations
import argparse
import logging
import numpy as np
import os
import pandas as pd
import time

from kaggler.data_io import load_data, save_data
from kaggler.preprocessing import LabelEncoder
from kaggler.feature_selection import DropInactive


imp_cat_cols = ['cat100', 'cat112', 'cat116', 'cat113', 'cat110',
                'cat101', 'cat80', 'cat114', 'cat79', 'cat91',
                'cat81', 'cat103', 'cat83', 'cat107', 'cat82',
                'cat111', 'cat105', 'cat84', 'cat93', 'cat115']


def generate_feature(train_file, test_file, train_feature_file,
                     test_feature_file, feature_map_file):
    logging.info('loading raw data')
    trn = pd.read_csv(train_file, index_col='id')
    tst = pd.read_csv(test_file, index_col='id')

    y = trn.loss.values
    n_trn = trn.shape[0]

    trn.drop('loss', axis=1, inplace=True)

    cat_cols = [x for x in trn.columns if trn[x].dtype == np.object]
    num_cols = [x for x in trn.columns if trn[x].dtype != np.object]

    logging.info('categorical: {}, numerical: {}'.format(len(cat_cols),
                                                         len(num_cols)))

    df = pd.concat([trn, tst], axis=0)

    logging.info('adding 2nd order interactions for categorical features')
    for i, (col1, col2) in enumerate(combinations(imp_cat_cols, 2), 1):
        col = '{}_{}'.format(col1, col2)
        df.ix[:, col] = df[col1] + df[col2]
        cat_cols.append(col)

        if (i % 10) == 0:
            logging.info('{} interactions processed'.format(i))

    logging.info('{} interactions processed'.format(i))
    logging.info('2nd order interaction categorical: {}'.format(len(cat_cols)))

    logging.info('label encoding categorical variables')
    lbe = LabelEncoder(min_obs=10)
    df.ix[:, cat_cols] = lbe.fit_transform(df[cat_cols].values)

    logging.info('training data shape: {}'.format(df.shape))
    with open(feature_map_file, 'w') as f:
        for i, col in enumerate(df.columns):
            f.write('{}\t{}\tq\n'.format(i, col))

    logging.info('saving features')
    save_data(df.values[:n_trn,], y, train_feature_file)
    save_data(df.values[n_trn:,], None, test_feature_file)


if __name__ == '__main__':

    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file', required=True, dest='train_file')
    parser.add_argument('--test-file', required=True, dest='test_file')
    parser.add_argument('--train-feature-file', required=True, dest='train_feature_file')
    parser.add_argument('--test-feature-file', required=True, dest='test_feature_file')
    parser.add_argument('--feature-map-file', required=True, dest='feature_map_file')

    args = parser.parse_args()

    start = time.time()
    generate_feature(args.train_file,
                     args.test_file,
                     args.train_feature_file,
                     args.test_feature_file,
                     args.feature_map_file)
    logging.info('finished ({:.2f} sec elasped)'.format(time.time() - start))

