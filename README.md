## [Internal Leaderboard](https://gitlab.com/jeongyoonlee/allstate-claims-severity/wikis/home)

## 2016-12-12 (post deadline)
* Finished 54th out of 3,091 teams on the private LB.

## 2016-12-12 (D-day)
* Improved the level-1 ensemble.
* Optimized the weighted average level-2 ensemble with ([esb.esb3](https://gitlab.com/jeongyoonlee/allstate-claims-severity/wikis/esb.esb3)).
* Optimal weights found by the Nelder-Mead method scored 1100.87.
* LB rank improved to 75

## 2016-12-09 (D-3)
* Started level-2 ensemble.  
* Weighted average of level-1 ensemble models ([esb.esb0](https://gitlab.com/jeongyoonlee/allstate-claims-severity/wikis/esb.esb0) and [esb.esb1](https://gitlab.com/jeongyoonlee/allstate-claims-severity/wikis/esb.esb1)) scored 1102.50 and 1102.41 respectively.
* LB rank improved to 17x

## 2016-12-04 (D-8)
* 10-fold-10-bag benchmark Keras model is finished and scored 1112.27.
* The average between the best XGB and Keras models scored 1103.50.
* LB rank improved to 226.

## 2016-11-30 (D-11)
* Added the first LightGBM(LBG) and XGB benchmark (1106.x) models, which scored 1113.40 and 1107.79 respectively.
* Adding two to ensemble ([esb8](https://gitlab.com/jeongyoonlee/allstate-claims-severity/wikis/esb8)) improved the LB score by 0.89 from 1105.78 to 1104.89
* LB rank dropped to 247
* My models scored consistently lower by around 1.0 than ones reported on the forum.  I suspect that my 5-fold CV instead of 10-fold CV is the reason.  Since I haven't trained many models so far, I will retrain all models with the 10-fold CV.
* Tried feature selection: selected top 100/300 features from best XGB models and trained a new XGB with selected features. It performed worse than original models.

## 2016-11-25 (D-16)
* First Keras model submission scored 1117.67, which is much worse than 1108.96, the score of the best XGB model. 
* Adding it to ensemble ([esb5](https://gitlab.com/jeongyoonlee/allstate-claims-severity/wikis/esb5)) improved the LB score significantly by 2.83 from 1108.76 to 1105.93.  
* LB rank improved from 330 to 208.

## 2016-11-23 (D-18)
* Added Keras and got errors after one epoch.  Need debug.
* Smaller eta in XGB keeps improving CV and LB.  So far eta of 0.01 gave the best performance

## 2016-11-20 (D-21)
* Retraining performed worse than the average of 5 fold predictions.
